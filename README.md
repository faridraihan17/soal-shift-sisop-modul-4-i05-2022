# Laporan Penjelasan Soal Shift Modul 3 
Sistem Operasi 2022 
## IUP 

#### Asisten : Restu Agung Parama
#### Anggota : 

<ul>
<li>
Farzana Afifah Razak - 5025201130 </li>
<li>Raihan Farid - 5025201141 </li>
<li>Rangga Aulia Pradana - 5025201154 </li>
</ul>



### Daftar isi
[TOC]


## Soal 1 
Penjelasan code soal nomor 1 :
```
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>

FILE *fileLog1;
FILE *fileLog2;
char dirpath[1001];

char *encodeAtBashNRot13(char *token) {
    int len = strlen(token);

    for(int i = 0; i < len; i++) {
        if(token[i] >= 65 && token[i] <= 90) {
            
            token[i] = 90 - (token[i] - 65);
        }
        else if(token[i] >= 97 && token[i] <= 122) {
            
            if(token[i] < 110)
                token[i] += 13;
            else
                token[i] -= 13;
        }
    }
    return token;
}
```
fitur mengatur penamaan file sesuai dengan ketentuan AtBashNRot13
```

char *decodeVigenereCipher(char *cipherText) {
    int i;
    char text;
    char plainText[1001];
    int textValue;
    char key[] = "INNUGANTENG";
    int len = strlen(key);
    memset(plainText, 0, sizeof(plainText));

    for (i = 0; i<strlen(cipherText); i++) {
        if (cipherText[i] >= 65 && cipherText[i] <= 90) {
            textValue = (((int)cipherText[i]-65) - (toupper(key[i%len])-65));
            while (textValue < 0) {
                textValue += 26;
            }
            textValue += 65; 
            text = (char)textValue;
        } else if (cipherText[i] >= 97 && cipherText[i] <= 122) {
            textValue = (((int)cipherText[i]-97) - (tolower(key[i%len])-97));
            while (textValue < 0) {
                textValue += 26;
            }
            textValue += 97;
            text = (char)textValue;
        } else
            text = cipherText[i];
        strncat(plainText, &text, 1);
    }
    strcpy(cipherText, plainText);
    return cipherText;
}

char *encodeFileIAN_(char *token) {
    char *ext = NULL;
    ext = strrchr(token, '.');

    char fileName[1001];
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;
        strncpy(fileName, token, length);
        fileName[length] = '\0';
        strcpy(fileName, encodeVigenereCipher(fileName));
        strcat(fileName, ext);
    } else {
        strcpy(fileName, encodeVigenereCipher(token));
    }
    strcpy(token, fileName);
    return token;
}

```
fitur encoding file IAN
```

char *decodeFileIAN_(char *token) {
    char *ext = NULL;
    ext = strrchr(token, '.');
    char fileName[1001];
    char result[1001] = "";
    char *pt = NULL;
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;

        strncpy(fileName, token, length);
        fileName[length] = '\0';
        
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName))) {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
        strcat(result, ext);
    }  else {
        strcpy(fileName, token);
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName))) {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
    }
    strcpy(token, result);
    return token;
}

```
fitur untuk melakukan decode file IAN
```
```

### Permasalahan soal nomor 1 :


## Soal 2 
Penjelasan code soal nomor 2 :
```

char *encodeFileAnimeku_(char *token) {
    char *ext = NULL;
    ext = strrchr(token, '.');

    char fileName[1001];
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;

        strncpy(fileName, token, length);
        fileName[length] = '\0';
        strcpy(fileName, encodeAtBashNRot13(fileName));
        strcat(fileName, ext);
    }
    else {
        strcpy(fileName, encodeAtBashNRot13(token));
    }

    strcpy(token, fileName);
    return token;
}

```
fitur melakukan pemberian nama pada file yg akan di encode
```
char *encodeVigenereCipher(char *plainText) {
    int i;
    char cipher;
    char cipherText[1001];
    int cipherValue;
    char key[] = "INNUGANTENG";
    int len = strlen(key);
    memset(cipherText, 0, sizeof(cipherText));

    for (i = 0; i<strlen(plainText); i++) {
        if (plainText[i] >= 65 && plainText[i] <= 90) {
            cipherValue = (((int)plainText[i]-65) + (toupper(key[i%len])-65)) % 26 + 65;
            cipher = (char)cipherValue;
        } else if(plainText[i] >= 97 && plainText[i] <= 122) {
            cipherValue = (((int)plainText[i]-97) + (tolower(key[i%len])-97)) % 26 + 97;
            cipher = (char)cipherValue;
        } else
            cipher = plainText[i];
        strncat(cipherText, &cipher, 1);
    }
    strcpy(plainText, cipherText);
    return plainText;
}

char *decodeVigenereCipher(char *cipherText) {
    int i;
    char text;
    char plainText[1001];
    int textValue;
    char key[] = "INNUGANTENG";
    int len = strlen(key);
    memset(plainText, 0, sizeof(plainText));

    for (i = 0; i<strlen(cipherText); i++) {
        if (cipherText[i] >= 65 && cipherText[i] <= 90) {
            textValue = (((int)cipherText[i]-65) - (toupper(key[i%len])-65));
            while (textValue < 0) {
                textValue += 26;
            }
            textValue += 65; 
            text = (char)textValue;
        } else if (cipherText[i] >= 97 && cipherText[i] <= 122) {
            textValue = (((int)cipherText[i]-97) - (tolower(key[i%len])-97));
            while (textValue < 0) {
                textValue += 26;
            }
            textValue += 97;
            text = (char)textValue;
        } else
            text = cipherText[i];
        strncat(plainText, &text, 1);
    }
    strcpy(cipherText, plainText);
    return cipherText;
}

char *encodeFileIAN_(char *token) {
    char *ext = NULL;
    ext = strrchr(token, '.');

    char fileName[1001];
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;
        strncpy(fileName, token, length);
        fileName[length] = '\0';
        strcpy(fileName, encodeVigenereCipher(fileName));
        strcat(fileName, ext);
    } else {
        strcpy(fileName, encodeVigenereCipher(token));
    }
    strcpy(token, fileName);
    return token;
}

```
fitur encoding file IAN
```

char *decodeFileIAN_(char *token) {
    char *ext = NULL;
    ext = strrchr(token, '.');
    char fileName[1001];
    char result[1001] = "";
    char *pt = NULL;
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;

        strncpy(fileName, token, length);
        fileName[length] = '\0';
        
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName))) {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
        strcat(result, ext);
    }  else {
        strcpy(fileName, token);
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName))) {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
    }
    strcpy(token, result);
    return token;
}

```
fitur untuk melakukan decode file IAN
```

char *find_path_Animeku_(char *pt1, char *token) {
    char temp[505] = "";
    char *pt2 = NULL;
    while((pt2 = strtok_r(pt1, "/", &pt1))) {
        char *pt3 = strstr(pt2, "Animeku_");
        if(pt3 == NULL)  {
            strcat(temp, "/");
            strcat(temp, pt2);
        }
    }
    strcpy(token, temp);
    printf("token %s temp %s\n", token, temp);
    return token;
}
```
fitur memeriksa apakah lokasi file tersedia
```

char *find_path_IAN_(char *pt1, char *token) {
    char temp[500] = "";
    char *pt2 = NULL;
    while((pt2 = strtok_r(pt1, "/", &pt1))) {
        char *pt3 = strstr(pt2, "IAN_");
        if(pt3 == NULL) {
            strcat(temp, "/");
            strcat(temp, pt2);
        }
    }
    strcpy(token, temp);
    return token;
}

```
fitur untuk memeriksa path file IAN
```

char *encodeSpecialFile(char *token) {
    int i, specialValue;
    char special;
    char *ext, charBinary[1001], 
    char *fileName[1001];
    ext = strrchr(token, '.');
    memset(charBinary, 0, sizeof(charBinary));
    memset(fileName, 0, sizeof(fileName));

    for (i = 0; i<strlen(token); i++) {
        if (token[i] == '.') 
            break;
        if (islower(token[i])){
            specialValue = (int)token[i] - 32;
            special = (char) specialValue;
            strcat(charBinary, "1");
        } else {
            special = token[i];
            strcat(charBinary, "0");
        }

        strncat(fileName, &special, 1);
    }
    if (ext)
        strcat(fileName, ext);
    strcat(fileName, ".");
 
    long int binary;
    binary = strtol(charBinary, NULL, 10);

    int decimal = 0, base = 1, rem;
    while (binary > 0) {
        rem = binary % 10;
        decimal = decimal + rem * base;
        binary = binary / 10;
        base = base * 2;
    }
    
    char decimalChar[1000];
    sprintf(decimalChar, "%d", decimal);
    strcat(fileName, decimalChar);
    strcpy(token, fileName);
    return token;
}

```
fitur untuk melakukan encode special file
```

char *encodeSpecialDir(char *token) {
    int i, specialValue;
    char special;
    char fileName[1000];
    memset(fileName, 0, sizeof(fileName));
    for (i = 0; i<strlen(token); i++) {
        if (token[i] == '.') break;
        if (islower(token[i])) {
            specialValue = (int)token[i] - 32;
            special = (char) specialValue;
        } else{
            special = token[i];
        }

        strncat(fileName, &special, 1);
    }
    strcpy(token, fileName);
    return token;
}
```
fitur untuk melakukan encode special directory
```

char *find_path(const char *path) {
    char fpath[2000];
    char unconstPath[500];
    char encodeString[500];
    strcpy(unconstPath, path);
    char *ptAnimeku_ = strstr(path, "Animeku_");
    char *ptIAN_ = strstr(path, "IAN_");
    if(ptAnimeku_) {
        find_path_Animeku_(ptAnimeku_, encodeString);
        encodeFileAnimeku_(encodeString);
        strcpy(unconstPath, path);
        strcat(unconstPath, encodeString);
        strcpy(fpath, unconstPath);
    } else if(ptIAN_) {
        find_path_IAN_(ptIAN_, encodeString);
        decodeFileIAN_(encodeString);
        strcpy(unconstPath, path);
        strcat(unconstPath, encodeString);
        strcpy(fpath, unconstPath);
    }
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, unconstPath);
    char *fpathPointer= fpath;
    return fpathPointer;
}
```
fitur memeriksa apakah path file sudah sesuai
```

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char logText[3000];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(logText, "INFO::%02d%02d%d-%02d:%02d:%02d::READDIR::%s\n", tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900, tm.tm_hour, tm.tm_min, tm.tm_sec, path);
    fputs(logText, fileLog2);

    char fpath[1000];
    strcpy(fpath, find_path(path));
    int res = 0;
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;
    while ((de = readdir(dp)) != NULL)  {
        if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) 
            continue;

        struct stat st;
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char *ptAnimeku_ = strstr(path, "Animeku_");
        char *ptIAN_ = strstr(path, "IAN_");
        char *ptNamDoSaq_ = strstr(path, "nam_do-saq_");
        if (ptAnimeku_ && !ptIAN_){
            if (de->d_type & DT_DIR){
             res = (filler(buf, encodeAtBashNRot13(de->d_name), & st, 0));
            } else {
                encodeFileAnimeku_(de->d_name);
                res = filler(buf, de->d_name, &st, 0);
            }
        }
        else if(ptIAN_ && !ptAnimeku_)   {
            if (de->d_type & DT_DIR){
                res = (filler(buf, encodeVigenereCipher(de->d_name), & st, 0));
            } else {
                encodeFileIAN_(de->d_name);
                res = filler(buf, de->d_name, &st, 0);
            }
        } else if (ptNamDoSaq_) {
            if (de->d_type & DT_DIR){
                res = (filler(buf, encodeSpecialDir(de->d_name), & st, 0));
            } else {
                encodeSpecialFile(de->d_name);
                res = filler(buf, de->d_name, &st, 0);
            }
        }   else {
            res = filler(buf, de->d_name, &st, 0);
        }      
        if(res!=0) break;
    }
    closedir(dp);
    return 0;
}

int  main(int  argc, char *argv[]) {
    char *username = getenv("USER");
    char logPathSoal1[1001];
    char logPathSoal2[1001];

    sprintf(dirpath, "/home/%s/Documents", username);
    sprintf(logPathSoal1, "/home/%s/Wibu.log", username);
    sprintf(logPathSoal2, "/home/%s/hayolongapain_D10.log", username);
    
    fileLog1 = fopen(logPathSoal1, "w");
    fileLog2 = fopen(logPathSoal2, "w");
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}

```
### Permasalahan soal nomor 2 :


## Soal 3 
Penjalasan code soal  nomor 3 :

### Permasalahan soal nomor 3:
   - 
